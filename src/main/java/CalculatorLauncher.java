public class CalculatorLauncher {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        int sum = calculator.addition(2, 1);
        System.out.println(sum);
        int sum2 = calculator.subtraction(2,1);
        System.out.println(sum2);
        int prod = calculator.multiplication(2,2);
        System.out.println(prod);
        int res = calculator.division(8, 4);
        System.out.println(res);
        int res2 = calculator.division(8, 0);
        System.out.println(res2);
    }
}