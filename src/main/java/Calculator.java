public class Calculator {

    public int addition(int a, int b) {
        int sum;
        sum = a + b;
        return sum;
    }

    public int subtraction(int a, int b) {
        int diff;
        diff = a - b;
        return diff;
    }

    public int multiplication(int a, int b) {
        int prod;
        prod = a * b;
        return prod;
    }

    public int division(int a, int b) {
        int res;
        try {
            res = a / b;
        } catch (ArithmeticException e) {
            System.out.println("Dzielenie przez zero niedozwolone");
            res = 0;
        }
        return res;
    }
}
